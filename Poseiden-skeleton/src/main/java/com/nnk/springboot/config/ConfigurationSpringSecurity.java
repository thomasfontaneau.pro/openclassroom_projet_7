package com.nnk.springboot.config;

import com.nnk.springboot.service.UserSecurityService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
@EnableWebSecurity
@EnableMethodSecurity
public class ConfigurationSpringSecurity {

  private final UserSecurityService userSecurityService;

  public ConfigurationSpringSecurity(UserSecurityService userSecurityService) {
    this.userSecurityService = userSecurityService;
  }

  /**
   * Permet la mise en place de filtre qui vont empêcher d'aller sur certaine page si l'utilisateur n'est pas connecté.
   */
  @Bean
  SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
    return http.authorizeHttpRequests( auth -> {
              auth.requestMatchers("/login").permitAll();
              auth.requestMatchers("/register").permitAll();
              auth.requestMatchers("/static/**").permitAll();
              auth.requestMatchers("/template/**").permitAll();
              auth.requestMatchers("/").permitAll();
              auth.anyRequest().authenticated();
            }).formLogin((form) -> form
                    .loginPage("/login")
                    .permitAll().defaultSuccessUrl("/bidList/list", true)
            )
            .userDetailsService(userSecurityService).build();
  }

  /**
   * Permet d'encrypter le mot de passe pour qu'il n'apparaisse pas en clair dans la db
   */
  @Bean
  PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder();
  }
}
