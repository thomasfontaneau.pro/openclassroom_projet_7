package com.nnk.springboot.repositories;

import com.nnk.springboot.domain.BidList;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface BidListRepository extends JpaRepository<BidList, Integer> {

  @Query("SELECT bl FROM BidList bl")
  Iterable<BidList> allBidList();

  @Modifying
  @Query("UPDATE BidList bl SET bl.account = :#{#bidList.account}, bl.type = :#{#bidList.type}, bl.bidQuantity = :#{#bidList.bidQuantity} WHERE bl.id = :id")
  void upadeteBidList(@Param("bidList")BidList bidList,@Param("id") Integer bidListId);
}
