package com.nnk.springboot.repositories;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.domain.CurvePoint;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;


public interface CurvePointRepository extends JpaRepository<CurvePoint, Integer> {

  @Modifying
  @Query("UPDATE CurvePoint cp SET cp.term = :#{#curvePoint.term}, cp.value = :#{#curvePoint.value} WHERE cp.id = :id")
  void upadeteBidList(@Param("curvePoint") CurvePoint curvePoint, @Param("id") Integer id);
}
