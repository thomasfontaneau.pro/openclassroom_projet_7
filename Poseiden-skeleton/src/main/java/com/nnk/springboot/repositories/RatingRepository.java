package com.nnk.springboot.repositories;

import com.nnk.springboot.domain.CurvePoint;
import com.nnk.springboot.domain.Rating;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RatingRepository extends JpaRepository<Rating, Integer> {

  @Modifying
  @Query("UPDATE Rating rt SET rt.fitchRating = :#{#rating.fitchRating}, rt.moodysRating = :#{#rating.moodysRating}, rt.orderNumber = :#{#rating.orderNumber}, rt.sandPRating = :#{#rating.sandPRating} WHERE rt.id = :id")
  void updateRating(Rating rating, Integer id);
}
