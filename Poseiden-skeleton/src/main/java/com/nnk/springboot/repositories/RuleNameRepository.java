package com.nnk.springboot.repositories;

import com.nnk.springboot.domain.Rating;
import com.nnk.springboot.domain.RuleName;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;


public interface RuleNameRepository extends JpaRepository<RuleName, Integer> {

  @Modifying
  @Query("UPDATE RuleName rm SET rm.name = :#{#ruleName.name}, rm.description = :#{#ruleName.description}, rm.json = :#{#ruleName.json}, rm.template = :#{#ruleName.template}, rm.sqlPart = :#{#ruleName.sqlPart}, rm.sqlStr = :#{#ruleName.sqlStr} WHERE rm.id = :id")
  void updateRuleName(RuleName ruleName, Integer id);
}
