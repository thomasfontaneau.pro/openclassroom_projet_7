package com.nnk.springboot.repositories;

import com.nnk.springboot.domain.RuleName;
import com.nnk.springboot.domain.Trade;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;


public interface TradeRepository extends JpaRepository<Trade, Integer> {
  @Modifying
  @Query("UPDATE Trade tr SET tr.account = :#{#trade.account}, tr.type = :#{#trade.type}, tr.buyQuantity = :#{#trade.buyQuantity} WHERE tr.id = :id")
  void updateTrade(Trade trade, Integer id);
}