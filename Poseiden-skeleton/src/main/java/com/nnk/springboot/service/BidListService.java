package com.nnk.springboot.service;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.repositories.BidListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Collections;
import java.util.Optional;

@Service
@Transactional
public class BidListService {

  @Autowired
  private BidListRepository bidListRepository;

  public Iterable<BidList> getBidList() {
    return bidListRepository.allBidList();
  }

  public Optional<BidList> getBidListById(Integer id) {
    return bidListRepository.findById(id);
  }

  public void updateBidList(BidList bidList, Integer id) {
    bidListRepository.upadeteBidList(bidList, id);
  }

  public void saveBidList(BidList bid) {
    bidListRepository.save(bid);
  }

  public void deleteBidList(Integer id) {
    bidListRepository.deleteById(id);
  }
}
