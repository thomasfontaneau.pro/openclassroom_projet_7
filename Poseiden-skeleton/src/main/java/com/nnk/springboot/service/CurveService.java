package com.nnk.springboot.service;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.domain.CurvePoint;
import com.nnk.springboot.repositories.CurvePointRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.Optional;

@Service
@Transactional
public class CurveService {

  @Autowired
  private CurvePointRepository curvePointRepository;

  public Object getCurveList() {
    return curvePointRepository.findAll();
  }

  public void saveCurvePoint(CurvePoint cp) {
    curvePointRepository.save(cp);
  }

  public Optional<CurvePoint> getCurvePointById(Integer id) {
    return curvePointRepository.findById(id);
  }

  public void updateCurvePoint(CurvePoint curvePoint, Integer id) {
    curvePointRepository.upadeteBidList(curvePoint, id);
  }

  public void deleteCurvePoint(Integer id) {
    curvePointRepository.deleteById(id);
  }

}
