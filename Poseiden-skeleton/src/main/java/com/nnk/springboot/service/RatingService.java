package com.nnk.springboot.service;

import com.nnk.springboot.domain.CurvePoint;
import com.nnk.springboot.domain.Rating;
import com.nnk.springboot.repositories.CurvePointRepository;
import com.nnk.springboot.repositories.RatingRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class RatingService {

  @Autowired
  private RatingRepository ratingRepository;

  public Object getRatings() {return ratingRepository.findAll();}

  public void saveRating(Rating rt) {
    ratingRepository.save(rt);
  }

  public Optional<Rating> getRatingById(Integer id) {
    return ratingRepository.findById(id);
  }

  public void deleteRating(Integer id) {
    ratingRepository.deleteById(id);
  }

  public void updateCurvePoint(Rating rating, Integer id) {
    ratingRepository.updateRating(rating, id);
  }
}
