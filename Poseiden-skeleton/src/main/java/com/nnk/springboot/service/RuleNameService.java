package com.nnk.springboot.service;

import com.nnk.springboot.domain.Rating;
import com.nnk.springboot.domain.RuleName;
import com.nnk.springboot.repositories.RatingRepository;
import com.nnk.springboot.repositories.RuleNameRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class RuleNameService {

  @Autowired
  private RuleNameRepository ruleNameRepository;

  public Object getRuleNames() {return ruleNameRepository.findAll();}

  public void saveRuleNames(RuleName rm) {
    ruleNameRepository.save(rm);
  }

  public Optional<RuleName> getRuleNameById(Integer id) {
    return ruleNameRepository.findById(id);
  }

  public void deleteRuleName(Integer id) {
    ruleNameRepository.deleteById(id);
  }

  public void updateRuleName(RuleName ruleName, Integer id) {
    ruleNameRepository.updateRuleName(ruleName, id);
  }
}
