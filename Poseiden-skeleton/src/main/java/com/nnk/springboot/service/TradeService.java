package com.nnk.springboot.service;

import com.nnk.springboot.controllers.TradeController;
import com.nnk.springboot.domain.RuleName;
import com.nnk.springboot.domain.Trade;
import com.nnk.springboot.repositories.TradeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service
@Transactional
public class TradeService {

  @Autowired
  private TradeRepository tradeRepository;

  public Object getTrades() {
    return tradeRepository.findAll();
  }

  public void saveTrades(Trade td) {
    tradeRepository.save(td);
  }

  public Optional<Trade> getTradeById(Integer id) {
    return tradeRepository.findById(id);
  }

  public void deleteTrade(Integer id) {
    tradeRepository.deleteById(id);
  }

  public void updateTrade(Trade trade, Integer id) {
    tradeRepository.updateTrade(trade, id);
  }
}
