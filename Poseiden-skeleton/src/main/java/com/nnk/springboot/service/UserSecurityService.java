package com.nnk.springboot.service;

import com.nnk.springboot.domain.User;
import com.nnk.springboot.repositories.UserRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class UserSecurityService implements UserDetailsService {

  private final UserRepository userRepository;

  public UserSecurityService(UserRepository userRepository) {
    this.userRepository = userRepository;
  }

  /**
   * Récupérer les informations d'un utilisateur à partir de la db.
   * @param username nom de l'utilisateur
   * @return données de connection de l'utilisateur
   * @throws UsernameNotFoundException si aucun utilisateur n'a été trouvé
   */
  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    Optional<User> optionalUser = userRepository.findByUsername(username);

    if (optionalUser.isPresent()) {
      User user = optionalUser.get();
      return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), new ArrayList<>());

    } else {
      throw new UsernameNotFoundException("Invalid username or password.");
    }
  }

  /**
   * Permet de récupérer les informations de l'utilisateur connecté
   * @param username nom de l'utilisateur
   * @return l'utilisateur connecté
   */
  public Optional<User> getUserConnect(String username) {
    return userRepository.findByUsername(username);
  }

}
