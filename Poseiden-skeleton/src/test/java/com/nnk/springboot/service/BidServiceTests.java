package com.nnk.springboot.service;

import com.nnk.springboot.domain.BidList;
import com.nnk.springboot.repositories.BidListRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeAll;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class BidServiceTests {

  @Autowired
  private BidListService bidListService;
  @MockBean
  private BidListRepository bidListRepository;

  static List<BidList> bidListTest = new ArrayList<>();

  @BeforeAll
  static void init() {
    BidList newBidList = new BidList();
    newBidList.setId(1);
    newBidList.setAccount("Compte absol");
    newBidList.setType("Enchère scellée");
    newBidList.setBidQuantity(5673.94);
    bidListTest.add(newBidList);

    BidList newBidList2 = new BidList();
    newBidList2.setId(2);
    newBidList2.setAccount("Compte absol");
    newBidList2.setType("Enchère à double tour");
    newBidList2.setBidQuantity(425.47);
    bidListTest.add(newBidList2);
  }

  @Test
  public void getBidListTest() {
    // GIVEN
    when(bidListRepository.allBidList()).thenReturn(bidListTest);

    // WHEN
    Object bidList = bidListService.getBidList();

    // THEN
    assertEquals(bidListTest, bidList);
  }

  @Test
  public void getBidListByIdTest() {
    // GIVEN
    when(bidListRepository.findById(1)).thenReturn(bidListTest.stream().filter(bidList -> bidList.getId() == 1).findFirst());

    // WHEN
    Optional<BidList> bidList = bidListService.getBidListById(1);

    // THEN
    assertEquals(bidListTest.stream().filter(bidList2 -> bidList2.getId() == 1).findFirst(), bidList);
  }

  @Test
  public void updateBidListTest() {
    // GIVEN
    BidList updateBidList = new BidList();
    updateBidList.setAccount("Compte absol");
    updateBidList.setType("Enchère à la criée");
    updateBidList.setBidQuantity(42553.47);

    // WHEN
    bidListService.updateBidList(updateBidList, 1);

    // THEN
    verify(bidListRepository).upadeteBidList(updateBidList, 1);
  }

  @Test
  public void saveBidListTest() {
    // GIVEN
    BidList newBidList3 = new BidList();
    newBidList3.setId(3);
    newBidList3.setAccount("Compte absol");
    newBidList3.setType("Enchère à la criée");
    newBidList3.setBidQuantity(42553.47);

    // WHEN
    bidListService.saveBidList(newBidList3);

    // THEN
    verify(bidListRepository).save(newBidList3);
  }

  @Test
  public void deleteBidListTest() {
    // WHEN
    bidListService.deleteBidList(1);

    // THEN
    verify(bidListRepository).deleteById(1);
  }
}
