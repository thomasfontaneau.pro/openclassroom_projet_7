package com.nnk.springboot.service;

import com.nnk.springboot.domain.CurvePoint;
import com.nnk.springboot.repositories.CurvePointRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class CurveServiceTests {

  @Autowired
  private CurveService curveService;
  @MockBean
  private CurvePointRepository curvePointRepository;

  static List<CurvePoint> curvePointsListTest = new ArrayList<>();

  @BeforeAll
  static void init() {
    CurvePoint newCurvePoint = new CurvePoint();
    newCurvePoint.setId(1);
    newCurvePoint.setTerm(3.74);
    newCurvePoint.setValue(3942.3);
    curvePointsListTest.add(newCurvePoint);

    CurvePoint newCurvePoint2 = new CurvePoint();
    newCurvePoint2.setId(2);
    newCurvePoint2.setTerm(5.32);
    newCurvePoint2.setTerm(344241.53);
    curvePointsListTest.add(newCurvePoint2);
  }

  @Test
  public void getCurveListTest() {
    // GIVEN
    when(curvePointRepository.findAll()).thenReturn(curvePointsListTest);

    // WHEN
    Object curvePointsList = curveService.getCurveList();

    // THEN
    assertEquals(curvePointsListTest, curvePointsList);
  }

  @Test
  public void getCurvePointByIdTest() {
    // GIVEN
    when(curvePointRepository.findById(1)).thenReturn(curvePointsListTest.stream().filter(curvePoint -> curvePoint.getId() == 1).findFirst());

    // WHEN
    Optional<CurvePoint> curvePoint = curveService.getCurvePointById(1);

    // THEN
    assertEquals(curvePointsListTest.stream().filter(curvePoint2 -> curvePoint2.getId() == 1).findFirst(), curvePoint);
  }

  @Test
  public void updateCurvePointTest() {
    // GIVEN
    CurvePoint updateCurvePoint = new CurvePoint();
    updateCurvePoint.setTerm(6.8);
    updateCurvePoint.setValue(3942.3);

    // WHEN
    curveService.updateCurvePoint(updateCurvePoint, 1);

    // THEN
    verify(curvePointRepository).upadeteBidList(updateCurvePoint, 1);
  }

  @Test
  public void saveBidListTest() {
    // GIVEN
    CurvePoint newCurvePoint3 = new CurvePoint();
    newCurvePoint3.setId(3);
    newCurvePoint3.setTerm(3.64);
    newCurvePoint3.setValue(39.3);

    // WHEN
    curveService.saveCurvePoint(newCurvePoint3);

    // THEN
    verify(curvePointRepository).save(newCurvePoint3);
  }

  @Test
  public void deleteBidListTest() {
    // WHEN
    curveService.deleteCurvePoint(1);

    // THEN
    verify(curvePointRepository).deleteById(1);
  }
}
