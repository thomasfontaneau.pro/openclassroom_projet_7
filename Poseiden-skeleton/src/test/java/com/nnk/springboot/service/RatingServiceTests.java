package com.nnk.springboot.service;

import com.nnk.springboot.domain.Rating;
import com.nnk.springboot.repositories.RatingRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles("test")
public class RatingServiceTests {

  @Autowired
  private RatingService ratingService;
  @MockBean
  private RatingRepository ratingRepository;

  static List<Rating> ratingListTest = new ArrayList<>();

  @BeforeAll
  static void init() {
    Rating newRating = new Rating();
    newRating.setId(1);
    newRating.setFitchRating("Test 1");
    newRating.setMoodysRating("Test 1");
    newRating.setSandPRating("Test 1");
    newRating.setOrderNumber(1);
    ratingListTest.add(newRating);

    Rating newRating2 = new Rating();
    newRating2.setId(1);
    newRating2.setFitchRating("Test 1");
    newRating2.setMoodysRating("Test 1");
    newRating2.setSandPRating("Test 1");
    newRating2.setOrderNumber(1);
    ratingListTest.add(newRating2);
  }

  @Test
  public void getRatingListTest() {
    // GIVEN
    when(ratingRepository.findAll()).thenReturn(ratingListTest);

    // WHEN
    Object ratingList = ratingService.getRatings();

    // THEN
    assertEquals(ratingListTest, ratingList);
  }

  @Test
  public void getRatingByIdTest() {
    // GIVEN
    when(ratingRepository.findById(1)).thenReturn(ratingListTest.stream().filter(rating -> rating.getId() == 1).findFirst());

    // WHEN
    Optional<Rating> rating = ratingService.getRatingById(1);

    // THEN
    assertEquals(ratingListTest.stream().filter(curvePoint2 -> curvePoint2.getId() == 1).findFirst(), rating);
  }

  @Test
  public void updateRatingTest() {
    // GIVEN
    Rating updateRating = new Rating();
    updateRating.setFitchRating("Test 1 update");
    updateRating.setMoodysRating("Test 1 update");
    updateRating.setSandPRating("Test 1 update");
    updateRating.setOrderNumber(5);

    // WHEN
    ratingService.updateCurvePoint(updateRating, 1);

    // THEN
    verify(ratingRepository).updateRating(updateRating, 1);
  }

  @Test
  public void saveBidListTest() {
    // GIVEN
    Rating newRating3 = new Rating();
    newRating3.setId(3);
    newRating3.setFitchRating("Test 3");
    newRating3.setMoodysRating("Test 3");
    newRating3.setSandPRating("Test 3");
    newRating3.setOrderNumber(3);

    // WHEN
    ratingService.saveRating(newRating3);

    // THEN
    verify(ratingRepository).save(newRating3);
  }

  @Test
  public void deleteBidListTest() {
    // WHEN
    ratingService.deleteRating(1);

    // THEN
    verify(ratingRepository).deleteById(1);
  }
}
